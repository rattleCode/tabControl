set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
package require Tk
package require tabControl 0.07
    # package require dicttool

    #
    # -- ttk::notebook
    #
variable ttkNotebook
pack [ttk::labelframe .lf  -text "ttk::notebook"] -fill x
set ttkNotebook [ttk::notebook .lf.nb -height 250]
pack $ttkNotebook -fill x
	$ttkNotebook add [frame  $ttkNotebook.f1  -bg white] -text "First tab"
	$ttkNotebook add [frame  $ttkNotebook.f2  -bg white] -text "Second tab"
	$ttkNotebook add [frame  $ttkNotebook.f3  -bg white] -text "Third tab"
	$ttkNotebook add [frame  $ttkNotebook.f4  -bg white] -text "Fourth tab"
	$ttkNotebook add [frame  $ttkNotebook.f5  -bg white] -text "Fifth tab"
	$ttkNotebook add [frame  $ttkNotebook.f6  -bg white] -text "Sixth tab"
	$ttkNotebook add [frame  $ttkNotebook.f7  -bg white] -text "Seventh tab"
	$ttkNotebook add [frame  $ttkNotebook.f8  -bg white] -text "Eighth tab"
    #
	$ttkNotebook select  $ttkNotebook.f2
    #
ttk::notebook::enableTraversal $ttkNotebook
    #
pack [button $ttkNotebook.f1.bt  -text "this is First Tab"   -width 35  -command [list addMessage "from First Button"]]
pack [button $ttkNotebook.f2.bt  -text "this is Second Tab"  -width 35  -command [list addMessage "from Second Button"]]
pack [button $ttkNotebook.f3.bt  -text "this is Third Tab"   -width 35  -command [list addMessage "from Third Button"]]
pack [button $ttkNotebook.f4.bt  -text "this is Fourth Tab"  -width 35  -command [list addMessage "from Fourth Button"]]
pack [button $ttkNotebook.f5.bt  -text "this is Fifth Tab"   -width 35  -command [list addMessage "from Fifth Button"]]
pack [button $ttkNotebook.f6.bt  -text "this is Sixth Tab"   -width 35  -command [list addMessage "from Sixth Button"]]
pack [button $ttkNotebook.f7.bt  -text "this is Seventh Tab" -width 35  -command [list addMessage "from Seventh Button"]]
pack [button $ttkNotebook.f8.bt  -text "this is Eighth Tab"  -width 35  -command [list addMessage "from Eighth Button"]]
    #
    #
pack [text   $ttkNotebook.f1.txt]
pack [text   $ttkNotebook.f2.txt]
pack [text   $ttkNotebook.f3.txt]
pack [text   $ttkNotebook.f4.txt]
pack [text   $ttkNotebook.f5.txt]
pack [text   $ttkNotebook.f6.txt]
pack [text   $ttkNotebook.f7.txt]
pack [text   $ttkNotebook.f8.txt]
    #
set textManual "please follow these instructions:\n   1. select Tab: \"Fourth Tab\"\n   2. press Button: \"visibility 001\"\n   3. press Button: \"visibility 002\"\n   4. select Tab: \"Second Tab\"\n   5. select Tab: \"Third Tab\""
    #
$ttkNotebook.f1.txt insert end  "This is Tab: 1\n\n$textManual"
$ttkNotebook.f2.txt insert end  "This is Tab: 2\n\n$textManual"
$ttkNotebook.f3.txt insert end  "This is Tab: 3\n\n$textManual"
$ttkNotebook.f4.txt insert end  "This is Tab: 4\n\n$textManual"
$ttkNotebook.f5.txt insert end  "This is Tab: 5\n\n$textManual"
$ttkNotebook.f6.txt insert end  "This is Tab: 6\n\n$textManual"
$ttkNotebook.f7.txt insert end  "This is Tab: 7\n\n$textManual"
$ttkNotebook.f8.txt insert end  "This is Tab: 8\n\n$textManual"    
    #
    
    #
    # -- tabControl
    #
variable objTabControl
pack [ttk::labelframe .lfTC  -text "tabControl Widget"] -fill x
set objTabControl   [tabControl::TabControl new .lfTC._tC]
set ctrlView        [$objTabControl getPath]
    # puts "  <D> $ctrlView"
grid columnconfigure .lfTC 0 -weight 1
grid $ctrlView      -sticky ew
    # -- occupy ttk::notebook
$objTabControl occupy $ttkNotebook    
    #

    #
    # -- manageControl
    #
variable manageControl native
pack [ttk::labelframe .mngControl  -text "manage Control"] -fill x
grid [radiobutton .mngControl.btnNative -text "native"   -variable manageControl -value "native"] -row 1 -column 1
grid [radiobutton .mngControl.btnTabControl -text "TabControl" -variable manageControl -value "TabControl"] -row 1 -column 2
grid [label .mngControl.myText   -text "     Status: "] -row 1 -column 3
grid [label .mngControl.myLabel  -text "native_" -textvariable manageControl] -row 1 -column 4
.mngControl.btnNative select
    #
    
    #
    # -- debug -- nativeControl
    #
pack [frame .f_control] -fill both -expand yes
    #
pack [ttk::labelframe .f_control.f_left  -text "control notebook"] -side left -fill y -expand yes
    #
pack [frame  .f_control.f_left.f_top]
    #
pack [frame  .f_control.f_left.f_top.f_left]    -side left
pack [button .f_control.f_left.f_top.f_left.bt_v01      -text "visibility 001"  -width 10  -command [list notebookVisibility_001]]      -side top
pack [button .f_control.f_left.f_top.f_left.bt_v02      -text "visibility 002"  -width 10  -command [list notebookVisibility_002]]      -side top
pack [button .f_control.f_left.f_top.f_left.bt_v03      -text "visibility 003"  -width 10  -command [list notebookVisibility_003]]      -side top
pack [button .f_control.f_left.f_top.f_left.bt_v04      -text "visibility 004"  -width 10  -command [list notebookVisibility_004]]      -side top
pack [button .f_control.f_left.f_top.f_left.bt_v05      -text "visibility 005"  -width 10  -command [list notebookVisibility_005]]      -side top
pack [button .f_control.f_left.f_top.f_left.bt_v99      -text "visibility 099"  -width 10  -command [list notebookVisibility_999]]      -side top
pack [button .f_control.f_left.f_top.f_left.bt_all      -text "visibility all"  -width 10  -command [list notebookVisibility_all]]      -side top
    #
pack [frame  .f_control.f_left.f_top.f_center]  -side left
pack [button .f_control.f_left.f_top.f_center.bt_s01    -text "select First"    -width 10  -command [list notebookSelect  $ttkNotebook.f1]]   -side top
pack [button .f_control.f_left.f_top.f_center.bt_s02    -text "select Second"   -width 10  -command [list notebookSelect  $ttkNotebook.f2]]   -side top
pack [button .f_control.f_left.f_top.f_center.bt_s03    -text "select Third"    -width 10  -command [list notebookSelect  $ttkNotebook.f3]]   -side top
pack [button .f_control.f_left.f_top.f_center.bt_s04    -text "select Fourth"   -width 10  -command [list notebookSelect  $ttkNotebook.f4]]   -side top
pack [button .f_control.f_left.f_top.f_center.bt_s05    -text "select Fifth"    -width 10  -command [list notebookSelect  $ttkNotebook.f5]]   -side top
pack [button .f_control.f_left.f_top.f_center.bt_s06    -text "select Sixth"    -width 10  -command [list notebookSelect  $ttkNotebook.f6]]   -side top
pack [button .f_control.f_left.f_top.f_center.bt_s07    -text "select Seventh"  -width 10  -command [list notebookSelect  $ttkNotebook.f7]]   -side top
pack [button .f_control.f_left.f_top.f_center.bt_s08    -text "select Eighth"   -width 10  -command [list notebookSelect  $ttkNotebook.f8]]   -side top
    #
pack [frame  .f_control.f_left.f_top.f_right]   -side right
pack [button .f_control.f_left.f_top.f_right.bt_h01     -text "hide First"      -width 10  -command [list notebookHide    $ttkNotebook.f1]]   -side top
pack [button .f_control.f_left.f_top.f_right.bt_h02     -text "hide Second"     -width 10  -command [list notebookHide    $ttkNotebook.f2]]   -side top
pack [button .f_control.f_left.f_top.f_right.bt_h03     -text "hide Third"      -width 10  -command [list notebookHide    $ttkNotebook.f3]]   -side top
pack [button .f_control.f_left.f_top.f_right.bt_h04     -text "hide Fourth"     -width 10  -command [list notebookHide    $ttkNotebook.f4]]   -side top
pack [button .f_control.f_left.f_top.f_right.bt_s05     -text "hide Fifth"      -width 10  -command [list notebookHide    $ttkNotebook.f5]]   -side top
pack [button .f_control.f_left.f_top.f_right.bt_s06     -text "hide Sixth"      -width 10  -command [list notebookHide    $ttkNotebook.f6]]   -side top
pack [button .f_control.f_left.f_top.f_right.bt_s07     -text "hide Seventh"    -width 10  -command [list notebookHide    $ttkNotebook.f7]]   -side top
pack [button .f_control.f_left.f_top.f_right.bt_s08     -text "hide Eighth"     -width 10  -command [list notebookHide    $ttkNotebook.f8]]   -side top
    #
    
    #
    # -- debug -- output
    #
pack [frame  .f_control.f_right]  -side right
pack [text   .f_control.f_right.txt]
    #
    #
    
    #
    # -- windowControl
    #
pack [ttk::labelframe .f_bottom   -text "windowControl"] -side bottom -fill x -expand yes
pack [button .f_bottom.bt_failure -text "-- failure in 8.6.12 on MacOS --"  -width 30  -command [list notebookFailure_MacOS]]   -side top -fill x
pack [button .f_bottom.bt_status  -text "Current Notebook Status"           -width 30  -command [list notebookStatus]]          -side top -fill x
pack [button .f_bottom.bt_runtime -text "tcl runtime Info"                  -width 30  -command [list runtimeInfo]]             -side top -fill x
pack [button .f_bottom.bt_visible -text "select 1st visible"                -width 30  -command [list selectFirstVisible]]      -side top -fill x
pack [button .f_bottom.bt_clear   -text "clear Text"                        -width 30  -command [list clearText]]               -side top -fill x
    #
    #
    #
proc notebookFailure_MacOS {} {
    variable ttkNotebook
    notebookSelect $ttkNotebook.f4
    notebookVisibility_002
}
    #
proc notebookHide {tab} {
        #
    variable manageControl
    variable objTabControl
    variable ttkNotebook
        #
    addMessage "    ... notebookHide: $manageControl"
        #
    set retValue {...}
    switch -exact $manageControl {
        native {
            $ttkNotebook hide $tab
            set retValue [$ttkNotebook select]
        }
        TabControl {
            $objTabControl hide $tab
            set retValue [$objTabControl select]
        }
    }
    addMessage "... notebookHide: current Tab: $retValue"
        #
}
    #
proc notebookSelect {tab} {
        #
    variable manageControl
    variable objTabControl
    variable ttkNotebook
        #
    addMessage "    ... notebookSelect: $manageControl"
        #
    set retValue {...}
    switch -exact $manageControl {
        native {
            $ttkNotebook select $tab
            set retValue [$ttkNotebook select]
        }
        TabControl {
            $objTabControl select $tab
            set retValue [$objTabControl select]
        }
    }
        #
    addMessage "... notebookSelect: current Tab: $retValue"
        #
}
    #
proc update_NotebookContent {dict} {
        #
    variable manageControl
    variable objTabControl
    variable ttkNotebook
        #
    addMessage "    ... update_NotebookContent: $manageControl"
        #
    switch -exact $manageControl {
        native {
            dict for {key tabDict} $dict {
                dict for {action tabs} $tabDict {
                    switch $action {
                        add {
                            addMessage "       ... add:  $tabs"
                            foreach tab $tabs { 
                                $ttkNotebook add  $ttkNotebook.$tab
                            }
                        }
                        hide {
                            addMessage "       ... hide: $tabs"
                            foreach tab $tabs { 
                                $ttkNotebook hide $ttkNotebook.$tab
                            }
                        }
                    }
                }
            }
        }
        TabControl {
            dict for {key tabDict} $dict {
                dict for {action tabs} $tabDict {
                    switch $action {
                        add {
                            addMessage "       ... add:  $tabs"
                            foreach tab $tabs { 
                                $objTabControl add  $ttkNotebook.$tab
                            }
                        }
                        hide {
                            addMessage "       ... hide: $tabs"
                            foreach tab $tabs { 
                                $objTabControl hide $ttkNotebook.$tab
                            }
                        }
                    }
                }
            }
        }
        default {
            addMessage "\n        ... exception: \"$manageControl\" ... unknown!\n"
        }
    }
        #
}
    #
    #
proc notebookVisibility_all {} {
        #
    addMessage "... notebookVisibility_099"
        #
    set tabDict {
        1 {add  {f1 f2 f3 f4 f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_999 {} {
        #
    addMessage "... notebookVisibility_099"
        #
    set tabDict {
        1 {hide {f1 f2 f3 f4 f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_001 {} {
        #
    addMessage "... notebookVisibility_001"
        #
    set tabDict {
        1 {add  {f1 f2 f3 f4}} 
        2 {hide {f5 f6 f7 f8}} 
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_002 {} {
        #
    addMessage "... notebookVisibility_002"
        #
    set tabDict {
        1 {hide {f1}} 
        2 {add  {f2 f3}} 
        3 {hide {f4}}
        4 {add  {f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_003 {} {
        #
    addMessage "... notebookVisibility_003"
        #
    set tabDict {
        1 {hide {f1}} 
        2 {add  {f2 f3 f4}} 
        3 {hide {f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_004 {} {
        #
    addMessage "... notebookVisibility_004"
        #
    set tabDict {
        1 {hide {f1 f2 f3}} 
        2 {add  {f4 f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_005 {} {
        #
    addMessage "... notebookVisibility_005"
        #
    set tabDict {
        1 {hide {f1 f2 f3 f4}} 
        2 {add  {f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
    #
proc notebookStatus {} {
        #
    variable manageControl
    variable objTabControl
    variable ttkNotebook
        #
    addMessage "... notebookStatus: current Tab -> $ttkNotebook: [$ttkNotebook select]"
    addMessage "... notebookStatus: current Tab -> $ttkNotebook: [$objTabControl select]"
        #
}
    #
proc runtimeInfo {} {
    clearText
    addMessage  "    TclTk Runtime:" 
    addMessage  "" 
    addMessage  "        patchlevel: [info patchlevel]" 
}
    #
proc selectFirstVisible {} {
    set currentTab  [$ttkNotebook select]
    addMessage "... selectFirstVisible: current Tab: [$ttkNotebook select]"
    if {$currentTab == {}} {
        set listActive {}
        addMessage "\n    ... no tab selected!\n"
        foreach tab [$ttkNotebook tabs] {
            set tabState [$ttkNotebook tab $tab -state]
            addMessage "     -> $tab -state:$tabState"
            if {$tabState == {normal}} {
                lappend listActive $tab
                addMessage "    01 \$listActive $listActive"
                break
            }
        }
        addMessage "    02 \$listActive $listActive"
        if {$listActive != {}} {
            set activeTab   [lindex $listActive 0]
            addMessage "\n    ... select first normal tab: $activeTab\n"
            $ttkNotebook select $activeTab
            notebookStatus
        }
    }
}
    #
proc clearText {} {
        #
    .f_control.f_right.txt delete 1.0 end
        #
}
    #
proc addMessage {text} {
        #
    puts "$text"
        #
    .f_control.f_right.txt insert end "$text\n"
        #
}
    #
