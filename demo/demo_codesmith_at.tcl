 #
 #   (c) Manfred ROSENBERGER 2022/07/20
 #
 #

set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"   ;# point to the directory including tabControl-package
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
        # ... any of this directories should point to the directory including tabControl-package
    puts "   -> auto_path   $dir"
}    
    #
package require Tk
package require tabControl 0.07
catch {
    package require ttkThemes
    ttk::setTheme clearlooks
}
    #
    #
set parentFrame [frame .f   -width 450  -height 150 -bg orange]
pack $parentFrame    
    #
    #
    # --- create a grid of frames
    #
frame   $parentFrame.f      -width 550  -height 300 -bg lightgreen
    #
frame   $parentFrame.f.f0   -width 550  -height  50 -bg lightgray 
frame   $parentFrame.f.f1_0 -width 100  -height  50 -bg gray        ;# 
frame   $parentFrame.f.f1_1 -width 450  -height 300 -bg lightblue   ;# will contain ttk::notebook
frame   $parentFrame.f.f2_0 -width 100  -height 300 -bg darkgray    ;# will contain demo-buttons
frame   $parentFrame.f.f3_1 -width 450  -height  50 -bg lightgray   ;# will contain tabControl
    #
grid $parentFrame.f                   -sticky nsew
    #
grid columnconfigure $parentFrame   0 -weight 1
grid rowconfigure    $parentFrame   0 -weight 1
    #
grid rowconfigure    $parentFrame.f 0 -weight 0
grid rowconfigure    $parentFrame.f 1 -weight 0
grid rowconfigure    $parentFrame.f 2 -weight 1
grid rowconfigure    $parentFrame.f 3 -weight 0
    #
grid columnconfigure $parentFrame.f 0 -weight 0
grid columnconfigure $parentFrame.f 1 -weight 1
    #
    #
grid $parentFrame.f.f0              -row 0 -column 0    -sticky ew      -columnspan 2   ;
    #
grid $parentFrame.f.f1_0            -row 1 -column 0    -sticky ew                      ;
grid $parentFrame.f.f1_1            -row 1 -column 1    -sticky nsew    -rowspan 2      ;# contains ttk::notebook
grid $parentFrame.f.f2_0            -row 2 -column 0    -sticky nsew    -rowspan 2      ;
grid $parentFrame.f.f3_1            -row 3 -column 1    -sticky ew                      ;# contains tabControl
    #
    #
    # -- create Demo-Buttons
    #
button $parentFrame.f.f2_0.bt_01 -text " - 1 - create ttk::notebook "               -state active    -command [list createNotebook]
pack   $parentFrame.f.f2_0.bt_01 -fill x 
button $parentFrame.f.f2_0.bt_02 -text " - 2 - create tabControl "                  -state disabled  -command [list createTabControl]
pack   $parentFrame.f.f2_0.bt_02 -fill x 
button $parentFrame.f.f2_0.bt_03 -text " - 3 - occupy ttk::notebook "               -state disabled  -command [list occupyTabControl]
pack   $parentFrame.f.f2_0.bt_03 -fill x 
button $parentFrame.f.f2_0.bt_04 -text " - 4 - tabControl expand to parent "        -state disabled  -command [list expandTabControl]
pack   $parentFrame.f.f2_0.bt_04 -fill x 
button $parentFrame.f.f2_0.bt_05 -text " - 5 - hide/show tabs from ttk::notebook "  -state disabled  -command [list switchNotebookTabs]
pack   $parentFrame.f.f2_0.bt_05 -fill x 
button $parentFrame.f.f2_0.bt_06 -text " - 6 - show tabs: First, Fourth -> Second " -state disabled  -command [list select_001]
pack   $parentFrame.f.f2_0.bt_06 -fill x 
button $parentFrame.f.f2_0.bt_07 -text " - 7 - show tabs: Second, Fourth -> Third " -state disabled  -command [list select_002]
pack   $parentFrame.f.f2_0.bt_07 -fill x 
button $parentFrame.f.f2_0.bt_08 -text " - 8 - show tabs: All -> First "            -state disabled  -command [list select_003]
pack   $parentFrame.f.f2_0.bt_08 -fill x 
    #
    #
    # -- create Demo-procedures
    #
proc createNotebook {} {
        #
    variable parentFrame    
    variable ttkNotebook    
        #
    set ttkNotebook [ttk::notebook  $parentFrame.f.f1_1.nb -width 450 -height 300]
    pack $ttkNotebook -fill x
        $ttkNotebook add   [frame  $ttkNotebook.f1  -bg white]        -text "First tab"
        $ttkNotebook add   [frame  $ttkNotebook.f2  -bg lightgray]    -text "Second tab"
        $ttkNotebook add   [frame  $ttkNotebook.f3  -bg darkgray]     -text "Third tab"
        $ttkNotebook add   [frame  $ttkNotebook.f4  -bg gray]         -text "Fourth tab"
        #
    pack [button $ttkNotebook.f1.bt  -text "this is First Tab"   -width 35  -command [list addMessage "from First Button"]]
    pack [button $ttkNotebook.f2.bt  -text "this is Second Tab"  -width 35  -command [list addMessage "from Second Button"]]
    pack [button $ttkNotebook.f3.bt  -text "this is Third Tab"   -width 35  -command [list addMessage "from Third Button"]]
    pack [button $ttkNotebook.f4.bt  -text "this is Fourth Tab"  -width 35  -command [list addMessage "from Fourth Button"]]
        #
    $parentFrame.f.f2_0.bt_01 configure -state disabled
    $parentFrame.f.f2_0.bt_02 configure -state active
        #
}
    #
proc createTabControl {} {
        #
    variable parentFrame    
    variable objTabControl    
        #
    set objTabControl   [tabControl::TabControl new $parentFrame.f.f3_1._tC -offsetLeft 30 -paddingText 10]
    set ctrlView        [$objTabControl getPath]
        # puts "  <D> $ctrlView"
    grid $ctrlView      -sticky ew
        #
    $parentFrame.f.f2_0.bt_02 configure -state disabled
    $parentFrame.f.f2_0.bt_03 configure -state active
        #
}
    #
proc occupyTabControl {} {
        #
    variable parentFrame    
    variable objTabControl
    variable ttkNotebook    
        #
    $objTabControl occupyWidget $ttkNotebook
        #
    $parentFrame.f.f2_0.bt_03 configure -state disabled
    $parentFrame.f.f2_0.bt_04 configure -state active
        #
}
    #
proc expandTabControl {} {
        #
    variable parentFrame    
        #
    grid columnconfigure $parentFrame.f.f3_1 0 -weight 1
        #
    $parentFrame.f.f2_0.bt_04 configure -state disabled
    $parentFrame.f.f2_0.bt_05 configure -state active
    $parentFrame.f.f2_0.bt_06 configure -state active
    $parentFrame.f.f2_0.bt_07 configure -state active
    $parentFrame.f.f2_0.bt_08 configure -state active
        #
}
    #
proc switchNotebookTabs {} {
        #
    variable parentFrame    
    variable objTabControl
        #
    $objTabControl switchRemoteControllerTabs
        #
}
    #
proc select_001 {} {
        #
    variable objTabControl
    variable parentFrame
    variable ttkNotebook    
        #
    set currentState [$objTabControl switchTabsState]
    set currentTab   [$objTabControl select]
    puts "        -> \$currentTab   $currentTab"
    puts "        -> \$currentState $currentState]"
        #
    set previousState   $currentState    
        #
    set manageState     $currentState
        #
    dict set manageState  $ttkNotebook.f1 -state normal
    dict set manageState  $ttkNotebook.f2 -state active
    dict set manageState  $ttkNotebook.f3 -state hidden
    dict set manageState  $ttkNotebook.f4 -state normal
        #
    puts "        -> \$manageState $manageState]"
        #
    $objTabControl switchTabsState $manageState
        #
}
    #
proc select_002 {} {
        #
    variable objTabControl
    variable parentFrame
    variable ttkNotebook    
        #
    set currentState [$objTabControl switchTabsState]
    set currentTab   [$objTabControl select]
    puts "        -> \$currentTab   $currentTab"
    puts "        -> \$currentState $currentState]"
        #
    set previousState   $currentState    
        #
    set manageState     $currentState
        #
    dict set manageState  $ttkNotebook.f1 -state hidden
    dict set manageState  $ttkNotebook.f2 -state normal
    dict set manageState  $ttkNotebook.f3 -state active
    dict set manageState  $ttkNotebook.f4 -state normal
        #
    puts "        -> \$manageState $manageState]"
        #
    $objTabControl switchTabsState $manageState
        #
}
    #
proc select_003 {} {
        #
    variable objTabControl
    variable parentFrame
    variable ttkNotebook    
        #
    set currentState [$objTabControl switchTabsState]
    set currentTab   [$objTabControl select]
    puts "        -> \$currentTab   $currentTab"
    puts "        -> \$currentState $currentState]"
        #
    set previousState   $currentState    
        #
    set manageState     $currentState
        #
    dict set manageState  $ttkNotebook.f1 -state active
    dict set manageState  $ttkNotebook.f2 -state normal
    dict set manageState  $ttkNotebook.f3 -state normal
    dict set manageState  $ttkNotebook.f4 -state normal
        #
    puts "        -> \$manageState $manageState]"
        #
    $objTabControl switchTabsState $manageState
        #
}
    #
proc addMessage {txt} {
    puts "   -> $txt"
}   
    #