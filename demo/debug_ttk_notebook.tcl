    #
package require Tk
    #
pack [ttk::labelframe .lf  -text "ttk::notebook"] -fill x
pack [ttk::notebook  .lf.nb -height 250] -fill x
	.lf.nb add [frame  .lf.nb.f1  -bg white] -text "First tab"
	.lf.nb add [frame  .lf.nb.f2  -bg white] -text "Second tab"
	.lf.nb add [frame  .lf.nb.f3  -bg white] -text "Third tab"
	.lf.nb add [frame  .lf.nb.f4  -bg white] -text "Fourth tab"
	.lf.nb add [frame  .lf.nb.f5  -bg white] -text "Fifth tab"
	.lf.nb add [frame  .lf.nb.f6  -bg white] -text "Sixth tab"
	.lf.nb add [frame  .lf.nb.f7  -bg white] -text "Seventh tab"
	.lf.nb add [frame  .lf.nb.f8  -bg white] -text "Eighth tab"
    #
	.lf.nb select  .lf.nb.f2
    #
ttk::notebook::enableTraversal .lf.nb
    #
pack [button .lf.nb.f1.bt  -text "this is First Tab"   -width 35  -command [list addMessage "from First Button"]]
pack [button .lf.nb.f2.bt  -text "this is Second Tab"  -width 35  -command [list addMessage "from Second Button"]]
pack [button .lf.nb.f3.bt  -text "this is Third Tab"   -width 35  -command [list addMessage "from Third Button"]]
pack [button .lf.nb.f4.bt  -text "this is Fourth Tab"  -width 35  -command [list addMessage "from Fourth Button"]]
pack [button .lf.nb.f5.bt  -text "this is Fifth Tab"   -width 35  -command [list addMessage "from Fifth Button"]]
pack [button .lf.nb.f6.bt  -text "this is Sixth Tab"   -width 35  -command [list addMessage "from Sixth Button"]]
pack [button .lf.nb.f7.bt  -text "this is Seventh Tab" -width 35  -command [list addMessage "from Seventh Button"]]
pack [button .lf.nb.f8.bt  -text "this is Eighth Tab"  -width 35  -command [list addMessage "from Eighth Button"]]
    #
pack [frame  .f_left]   -side left
    #
pack [frame  .f_left.f_top]
    #
pack [frame  .f_left.f_top.f_left]    -side left
pack [button .f_left.f_top.f_left.bt_v01      -text "visibility 001"  -width 10  -command [list notebookVisibility_001]]      -side top
pack [button .f_left.f_top.f_left.bt_v02      -text "visibility 002"  -width 10  -command [list notebookVisibility_002]]      -side top
pack [button .f_left.f_top.f_left.bt_v03      -text "visibility 003"  -width 10  -command [list notebookVisibility_003]]      -side top
pack [button .f_left.f_top.f_left.bt_v04      -text "visibility 004"  -width 10  -command [list notebookVisibility_004]]      -side top
pack [button .f_left.f_top.f_left.bt_v05      -text "visibility 005"  -width 10  -command [list notebookVisibility_005]]      -side top
pack [button .f_left.f_top.f_left.bt_v99      -text "visibility 099"  -width 10  -command [list notebookVisibility_999]]      -side top
pack [button .f_left.f_top.f_left.bt_all      -text "visibility all"  -width 10  -command [list notebookVisibility_all]]      -side top
    #
pack [frame  .f_left.f_top.f_center]  -side left
pack [button .f_left.f_top.f_center.bt_s01    -text "select First"    -width 10  -command [list notebookSelect  .lf.nb.f1]]   -side top
pack [button .f_left.f_top.f_center.bt_s02    -text "select Second"   -width 10  -command [list notebookSelect  .lf.nb.f2]]   -side top
pack [button .f_left.f_top.f_center.bt_s03    -text "select Third"    -width 10  -command [list notebookSelect  .lf.nb.f3]]   -side top
pack [button .f_left.f_top.f_center.bt_s04    -text "select Fourth"   -width 10  -command [list notebookSelect  .lf.nb.f4]]   -side top
pack [button .f_left.f_top.f_center.bt_s05    -text "select Fifth"    -width 10  -command [list notebookSelect  .lf.nb.f5]]   -side top
pack [button .f_left.f_top.f_center.bt_s06    -text "select Sixth"    -width 10  -command [list notebookSelect  .lf.nb.f6]]   -side top
pack [button .f_left.f_top.f_center.bt_s07    -text "select Seventh"  -width 10  -command [list notebookSelect  .lf.nb.f7]]   -side top
pack [button .f_left.f_top.f_center.bt_s08    -text "select Eighth"   -width 10  -command [list notebookSelect  .lf.nb.f8]]   -side top
    #
pack [frame  .f_left.f_top.f_right]   -side right
pack [button .f_left.f_top.f_right.bt_h01     -text "hide First"      -width 10  -command [list notebookHide    .lf.nb.f1]]   -side top
pack [button .f_left.f_top.f_right.bt_h02     -text "hide Second"     -width 10  -command [list notebookHide    .lf.nb.f2]]   -side top
pack [button .f_left.f_top.f_right.bt_h03     -text "hide Third"      -width 10  -command [list notebookHide    .lf.nb.f3]]   -side top
pack [button .f_left.f_top.f_right.bt_h04     -text "hide Fourth"     -width 10  -command [list notebookHide    .lf.nb.f4]]   -side top
pack [button .f_left.f_top.f_right.bt_s05     -text "hide Fifth"      -width 10  -command [list notebookHide    .lf.nb.f5]]   -side top
pack [button .f_left.f_top.f_right.bt_s06     -text "hide Sixth"      -width 10  -command [list notebookHide    .lf.nb.f6]]   -side top
pack [button .f_left.f_top.f_right.bt_s07     -text "hide Seventh"    -width 10  -command [list notebookHide    .lf.nb.f7]]   -side top
pack [button .f_left.f_top.f_right.bt_s08     -text "hide Eighth"     -width 10  -command [list notebookHide    .lf.nb.f8]]   -side top
   #
pack [frame  .f_left.f_bottom]
    #
pack [button .f_left.f_bottom.bt_status  -text "Current Status"     -width 30  -command [list notebookStatus]]                -side top
pack [button .f_left.f_bottom.bt_visible -text "select 1st visible" -width 30  -command [list selectFirstVisible]]            -side top
pack [button .f_left.f_bottom.bt_clear   -text "clear Text"         -width 30  -command [list clearText]]                     -side top
pack [button .f_left.f_bottom.bt_failure -text "-- failure --"      -width 30  -command [list notebookFailure_MacOS]]         -side top
    #
pack [frame  .f_right]  -side right
pack [text   .f_right.txt]
    #
    #
pack [text   .lf.nb.f1.txt]
pack [text   .lf.nb.f2.txt]
pack [text   .lf.nb.f3.txt]
pack [text   .lf.nb.f4.txt]
pack [text   .lf.nb.f5.txt]
pack [text   .lf.nb.f6.txt]
pack [text   .lf.nb.f7.txt]
pack [text   .lf.nb.f8.txt]
    #
set textManual "please follow these instructions:\n   1. select Tab: \"Fourth Tab\"\n   2. press Button: \"visibility 001\"\n   3. press Button: \"visibility 002\"\n   4. select Tab: \"Second Tab\"\n   5. select Tab: \"Third Tab\""
    #
.lf.nb.f1.txt insert end  "This is Tab: 1\n\n$textManual"
.lf.nb.f2.txt insert end  "This is Tab: 2\n\n$textManual"
.lf.nb.f3.txt insert end  "This is Tab: 3\n\n$textManual"
.lf.nb.f4.txt insert end  "This is Tab: 4\n\n$textManual"
.lf.nb.f5.txt insert end  "This is Tab: 5\n\n$textManual"
.lf.nb.f6.txt insert end  "This is Tab: 6\n\n$textManual"
.lf.nb.f7.txt insert end  "This is Tab: 7\n\n$textManual"
.lf.nb.f8.txt insert end  "This is Tab: 8\n\n$textManual"
    #
proc notebookFailure_MacOS {} {
    # notebookSelect .lf.nb.f3
    notebookSelect .lf.nb.f4
    # notebookVisibility_001
    notebookVisibility_002
}
    #
proc notebookVisibility_all {} {
        #
    addMessage "... notebookVisibility_099"
        #
    set tabDict {
        1 {add  {f1 f2 f3 f4 f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_999 {} {
        #
    addMessage "... notebookVisibility_099"
        #
    set tabDict {
        1 {hide {f1 f2 f3 f4 f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_001 {} {
        #
    addMessage "... notebookVisibility_001"
        #
    set tabDict {
        1 {add  {f1 f2 f3 f4}} 
        2 {hide {f5 f6 f7 f8}} 
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_002 {} {
        #
    addMessage "... notebookVisibility_002"
        #
    set tabDict {
        1 {hide {f1}} 
        2 {add  {f2 f3}} 
        3 {hide {f4}}
        4 {add  {f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_003 {} {
        #
    addMessage "... notebookVisibility_003"
        #
    set tabDict {
        1 {hide {f1}} 
        2 {add  {f2 f3 f4}} 
        3 {hide {f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_004 {} {
        #
    addMessage "... notebookVisibility_004"
        #
    set tabDict {
        1 {hide {f1 f2 f3}} 
        2 {add  {f4 f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookVisibility_005 {} {
        #
    addMessage "... notebookVisibility_005"
        #
    set tabDict {
        1 {hide {f1 f2 f3 f4}} 
        2 {add  {f5 f6 f7 f8}}
    }
        #
    update_NotebookContent $tabDict
        #
}
    #
proc notebookHide {tab} {
    .lf.nb hide $tab
    set retValue [.lf.nb select]
    addMessage "... notebookSelect: current Tab: $retValue"
}
    #
proc notebookSelect {tab} {
    .lf.nb select $tab
    set retValue [.lf.nb select]
    addMessage "... notebookSelect: current Tab: $retValue"
}
    #
proc notebookStatus {} {
    set currentTab  [.lf.nb select]
    addMessage "... notebookStatus: current Tab: [.lf.nb select]"
}
    #
proc update_NotebookContent {dict} {
        #
    dict for {key tabDict} $dict {
        dict for {action tabs} $tabDict {
            switch $action {
                add {
                    addMessage "       ... add:  $tabs"
                    foreach tab $tabs { 
                        .lf.nb add  .lf.nb.$tab
                    }
                }
                hide {
                    addMessage "       ... hide: $tabs"
                    foreach tab $tabs { 
                        .lf.nb hide .lf.nb.$tab
                    }
                }
            }
        }
    }
        #
}
    #
proc selectFirstVisible {} {
    set currentTab  [.lf.nb select]
    addMessage "... selectFirstVisible: current Tab: [.lf.nb select]"
    if {$currentTab == {}} {
        set listActive {}
        addMessage "\n    ... no tab selected!\n"
        foreach tab [.lf.nb tabs] {
            set tabState [.lf.nb tab $tab -state]
            addMessage "     -> $tab -state:$tabState"
            if {$tabState == {normal}} {
                lappend listActive $tab
                addMessage "    01 \$listActive $listActive"
                break
            }
        }
        addMessage "    02 \$listActive $listActive"
        if {$listActive != {}} {
            set activeTab   [lindex $listActive 0]
            addMessage "\n    ... select first normal tab: $activeTab\n"
            .lf.nb select $activeTab
            notebookStatus
        }
    }
}
    #
proc clearText {} {
        #
    .f_right.txt delete 1.0 end
        #
}
    #
proc addMessage {text} {
        #
    puts "$text"
        #
    .f_right.txt insert end "$text\n"
        #
}
    #
