 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2022
 #
 # ----------------------------------------------------------------------
 #  namespace:  tabControl
 # ----------------------------------------------------------------------
 #
 #  0.00 - 2022/07/13
 #      ... 
 #

    #
oo::class create tabControl::TabView {
        #
    superclass tabControl::Observer
        #
    variable ObjectPathFrame
        #
    variable ObjState
        #
    variable TypeControl
    variable CurrentIndex
    variable DictRegistryCanvas
    variable DictRegistryButton
        #
    variable ArrayColor_Template
    variable ArrayTheme
    variable CurrentTheme
    variable DictThemeSettings
        #
    variable SlantWidth
    variable TextPadding
    variable OffsetLeft
        #
    variable xNext
    variable idNext
        #
    variable TabsFrame_Canvas
    variable TabsFrame_Button
        #
    constructor {myFrame typeControl offsetLeft paddingText state} {
            #
        set ObjectPathFrame     $myFrame
            #
        set TypeControl         $typeControl
        set ObjState            $state
            #
        set DictRegistryCanvas  {}
        set DictRegistryButton  {}
        set DictThemeSettings   {}
            #
        set CurrentTheme        {}
            #  
        array set ArrayTheme    {}
            #
        set idNext               0
            #
        set SlantWidth           6
        set TextPadding         [expr {$paddingText > 0 ? $paddingText : 0}]
        set OffsetLeft          [expr {$offsetLeft  > 0 ? $offsetLeft  : 0}]
            #
        if {$TypeControl eq "tab"} {
                #
                # -- TabsFrame_Canvas --
                #
            if [namespace exists ::tkp] {
                puts "     ... PathCanvas"
                set TabsFrame_Canvas    [tkp::canvas $myFrame.cv -height 22 -highlightthickness 0]
            } else {
                puts "     ... Canvas"
                set TabsFrame_Canvas    [canvas      $myFrame.cv -height 22 -highlightthickness 0]
            }
            pack $TabsFrame_Canvas  -padx 0 -pady -0 -ipady 0 -fill x -expand yes -side top
                #
                #
        } else {
                #
                # -- TabsFrame_Button --
                #
            set TabsFrame_Button    [frame $myFrame.buttonBar -background $ArrayTheme(background)]
            pack $TabsFrame_Button  -padx 0 -pady 0 -ipady 0 -fill x -expand yes -side top
                #
                #
        }
            #
            #
            # -- regiser for update
            #
        $ObjState subscribe     [self]
            #
            #
    }
    
    destructor {
            # clean up once the widget get's destroyed
        catch {[self] destroy}
            #
    }
    
    method unknown {method args} {
        return -code error "method \"$method\" is unknown"
    }
    
    method updateState {args} {
            #
        # puts "    ... tabControl::TabView -> updateState: -> $args"
            #
        if {$TypeControl eq "tab"} {
                #
            my UpdateTabCanvas
            my UpdateTabRendering
                #
        } else {
                #
            my UpdateTabButton
                #
        }
            #
    }
    
    method select {id} {
            # puts "   ... select $id"
        $ObjState select $id
    }

    method event_MouseOver {tagDict mode args } {
            # puts "  event_MouseOver:"
            # puts "      \$tagDict $tagDict"
            # puts "        \$mode  $mode"
            # puts "        \$args  $args"
            #
        set tagPolygon [dict get $tagDict tagPolygon ]   
        set tagBorder  [dict get $tagDict tagBorder]    
            #
        switch -exact -- $mode {
            enter {
                my FormatCanvasItem  $tagPolygon -fill   $ArrayTheme(tabBackground_Hover)
                my FormatCanvasItem  $tagPolygon -stroke $ArrayTheme(tabBackground_Hover)
            }
            leave {
                my FormatCanvasItem  $tagPolygon -fill   $ArrayTheme(tabBackground_Active)
                my FormatCanvasItem  $tagPolygon -stroke $ArrayTheme(tabBackground_Active)
            }
            default {}
        }
    }    
        #
        #
    method UpdateTabCanvas {} {
            #
            # puts "     UpdateTabCanvas"
            #
            # puts "        \$TabsFrame_Canvas: [winfo class $TabsFrame_Canvas]"
            #
        my UpdateThemeSettings
            #
        $TabsFrame_Canvas delete all
        set DictRegistryCanvas {}
            #
        set tabDef  [$ObjState getMap_VisibleTabIDs]
        set currTab [$ObjState getCurrent_TabID]
            # puts "        .. \$tabDef $tabDef"
        foreach {window name} $tabDef {
            if {$window eq $currTab} {
                my CreateTab $window $name 1
            } else {
                my CreateTab $window $name
            }
        }
    }
    method CreateTab {w text {active 0}} {
            #
            # puts "     CreateTab: $w $text"
            #
        set myText          [my CreateTabText [string trim $text]]
        set bbText          [$TabsFrame_Canvas bbox $myText]
            #
        lassign $bbText x1 y1 x2 y2
        set widthText       [expr {$x2 - $x1}]
            # heightText    [expr {$y2 - $y1}]
        set xCenterText     [expr {0.5 * ($x1 + $x2)}]
        set yCenterText     [expr {0.5 * ($y1 + $y2)}]
            #
            #
        set myShapeDef      [my DefineTabShape $widthText $active]  
        set myPolygon       [my CreateTabPolygon $myShapeDef]
        set myBorder        [my CreateTabBorder  $myShapeDef]
        set bbBorder   [$TabsFrame_Canvas bbox $myBorder]
            #
        lassign $bbBorder  x1 y1 x2 y2
        set widthPolygon    [expr {$x2 - $x1}]
        set xCenterBorder   [expr {0.5 * ($x1 + $x2)}]
        set yCenterBorder   [expr {0.5 * ($y1 + $y2)}]
            #
        set myShape         [format {_shape_%s} $w]
        $TabsFrame_Canvas addtag $myShape withtag $myPolygon
        $TabsFrame_Canvas addtag $myShape withtag $myBorder
            #
        set myTab           [format {_tab_%s} $w]
        $TabsFrame_Canvas addtag $myTab withtag $myShape
        $TabsFrame_Canvas addtag $myTab withtag $myText
            #
            #
        $TabsFrame_Canvas move $myText  [expr {$xCenterBorder - $xCenterText}]  10
            #
            #
            # puts "<D> ... \$bbText $bbText  <-?-> \$bbBorder $bbBorder"
            # puts "<D> ... \$xCenterText $xCenterText  <-?-> \$xCenterBorder $xCenterBorder"
            #
            #
        set xPolygon    $x1
            #
        if {[dict size $DictRegistryCanvas] == 0} {
            set tabOffset   [expr {- $xPolygon + $OffsetLeft}]
        } else {
            set tabOffset   $xNext
        }
        set xNext       [expr {$tabOffset + $widthPolygon - ((2 * $SlantWidth) -2)}]    ;# set the global variable
            #
        $TabsFrame_Canvas raise $myText
            #
        $TabsFrame_Canvas move $myTab $tabOffset 0
            #
            #
        dict set DictRegistryCanvas $w [list name $text tagName $myTab tagText $myText tagPolygon $myPolygon tagBorder $myBorder width $widthPolygon]
            #
            #
        $TabsFrame_Canvas bind $myTab <ButtonPress> [list [self] select $w]
            #
    }
    
    method CreateTabText {text} {
            #
        return [$TabsFrame_Canvas create text 0 0  -text $text]
            #
        switch -exact [winfo class $TabsFrame_Canvas] {
            PathCanvas {
                return [$TabsFrame_Canvas create ptext 0 0 -text $text -fontsize 12 -textanchor c]
            }
            Canvas -
            default {
                return [$TabsFrame_Canvas create text 0 0  -text $text -font "Helvetica 10"]
            }
        }
    }
    
    method DefineTabShape {minWidth {active 0}} {
        set widthTop    [expr {$minWidth + 2 *($SlantWidth + $TextPadding)}]
        set widthBottom [expr {$minWidth + 2 * $TextPadding}]
        if $active {
            set p0  {0 0}
            set p1  [list $SlantWidth 19]
            set p2  [list [expr $widthBottom + $SlantWidth] 19]
            set p3  [list $widthTop 0]
        } else {
            set p0  {0 0}
            set p1  [list $SlantWidth 18]
            set p2  [list [expr $widthBottom + $SlantWidth] 18]
            set p3  [list $widthTop 0]
        }
        return [join "$p0 $p1 $p2 $p3"]
    }
    
    method CreateTabPolygon {shape} {
            #
        switch -exact [winfo class $TabsFrame_Canvas] {
            PathCanvas {
                return [$TabsFrame_Canvas create ppolygon $shape -strokewidth 0]
            }
            Canvas -
            default {
                return [$TabsFrame_Canvas create polygon  $shape -width 0]
            }
        }
            #
    }
    
    method CreateTabBorder {shape} {
            #
        switch -exact [winfo class $TabsFrame_Canvas] {
            PathCanvas {
                return [$TabsFrame_Canvas create polyline $shape]
            }
            Canvas -
            default {
                return [$TabsFrame_Canvas create line     $shape]
            }
        }
            #
    }
    
        #
    method UpdateThemeSettings {} {
            #
            # puts "\n--- UpdateThemeSettings ---\n"
            # parray ArrayColor_Template
        if {[ttk::style theme use] eq $CurrentTheme} {
            return
        }
            #
        ttk::style layout _Tabless_.TNotebook.Tab {{}}
            #
            #
        set CurrentTheme    [ttk::style theme use]
            #
        set DictThemeSettings {}
        foreach state {active disabled focus pressed selected background readonly alternate invalid hover} {
            foreach attribute {-background -bordercolor -darkcolor -foreground -lightcolor} {
                    #  -padding -tabmargins -tabposition
                set value [ttk::style lookup TNotebook $attribute  $state]
                dict set DictThemeSettings TNotebook $attribute $state $value
            }
        }
        foreach state {active disabled focus pressed selected background readonly alternate invalid hover} {
            foreach attribute {-background -bordercolor -font -foreground} {
                    #  -compound -expand -padding
                set value [ttk::style lookup TNotebook.Tab $attribute  $state]
                dict set DictThemeSettings TNotebook.Tab $attribute $state $value
            }
        }
            #
        set ArrayTheme(color_Background)        [dict get $DictThemeSettings TNotebook      -background  background]
        set ArrayTheme(fontColor_Active)        [dict get $DictThemeSettings TNotebook.Tab  -foreground  active]
        set ArrayTheme(fontColor_Selected)      [dict get $DictThemeSettings TNotebook.Tab  -foreground  selected]
        set ArrayTheme(tabBackground_Active)    [dict get $DictThemeSettings TNotebook.Tab  -background  active]
        set ArrayTheme(tabBackground_Selected)  [dict get $DictThemeSettings TNotebook.Tab  -background  selected]  ;# selected
        set ArrayTheme(tabBorderColor_Active)   [dict get $DictThemeSettings TNotebook.Tab  -bordercolor active]
        set ArrayTheme(tabBorderColor_Selected) [dict get $DictThemeSettings TNotebook.Tab  -bordercolor selected]
        set ArrayTheme(tabBackground_Hover)     [dict get $DictThemeSettings TNotebook.Tab  -background  hover]
        set ArrayTheme(tabFont_Active)          [dict get $DictThemeSettings TNotebook.Tab  -font        active]
            #
        if 0 {
            if {[catch {puts "[dict print $DictThemeSettings]"} eID]} {
                puts $DictThemeSettings
            }
        }
            #
            # parray ArrayTheme
            #
        foreach name {fontColor_Active fontColor_Selected tabBorderColor_Active tabBorderColor_Selected} {
            if {$ArrayTheme($name) == {}} {
                set ArrayTheme($name) black
            }
        }
            #
        my UpdateTabRendering    
            #
            # puts "\n"
    }
    
    method UpdateTabRendering {} {
            #
            # puts "     UpdateTabRendering"
            # puts $DictRegistryCanvas
        # puts "[dict print $DictThemeSettings]"
            #
        if {[dict size $DictRegistryCanvas] == 0} {
            return
        }
        if {[dict size $DictThemeSettings] == 0} {
            return
        }
            #
        set keyActiveWindow [$ObjState getCurrent_TabID]
        if {$keyActiveWindow == {}} {
            return
        }
            #
        $TabsFrame_Canvas configure -background $ArrayTheme(color_Background)
            #
        foreach {key keyDict} $DictRegistryCanvas {
                # puts "   -> \$keyDict $keyDict"
                #
            set tagName     [dict get $keyDict tagName]
            set tagtText    [dict get $keyDict tagText]
            set tagPolygon  [dict get $keyDict tagPolygon]
            set tagBorder   [dict get $keyDict tagBorder]
                #
            if {$key ne $keyActiveWindow} {
                    #
                my FormatCanvasItem $tagtText   -fill    $ArrayTheme(fontColor_Active)  -font $ArrayTheme(tabFont_Active)
                my FormatCanvasItem $tagPolygon -fill    $ArrayTheme(tabBackground_Active)  -stroke $ArrayTheme(tabBackground_Active)
                my FormatCanvasItem $tagBorder  -stroke  $ArrayTheme(tabBorderColor_Active)
                    #
                $TabsFrame_Canvas bind $tagName <Enter> [list [self] event_MouseOver $keyDict enter]
                $TabsFrame_Canvas bind $tagName <Leave> [list [self] event_MouseOver $keyDict leave]
                    #
            } else {
                    #
                my FormatCanvasItem $tagtText   -fill   $ArrayTheme(fontColor_Selected)  -font $ArrayTheme(tabFont_Active)
                my FormatCanvasItem $tagPolygon -fill   $ArrayTheme(tabBackground_Selected)  -stroke $ArrayTheme(tabBackground_Selected)
                my FormatCanvasItem $tagBorder  -stroke $ArrayTheme(tabBorderColor_Active)
                    #
            }
                #
            $TabsFrame_Canvas raise $tagName all
                #
        }
            #
        set activeTab   [dict get $DictRegistryCanvas $keyActiveWindow tagName]
        $TabsFrame_Canvas raise $activeTab all                
            #
    }
        #
    method FormatCanvasItem {item args} {
        set itemType    [$TabsFrame_Canvas type $item]
            # puts "  -- [winfo class $TabsFrame_Canvas] -> FormatCanvasItem $itemType: $item $args"
        switch -exact [winfo class $TabsFrame_Canvas] {
            PathCanvas {
                $TabsFrame_Canvas itemconfigure  $item   {*}$args
            }
            Canvas {
                    # puts "        -> \$itemType: $itemType"
                switch -exact -- $itemType {
                    polygon {
                        set args [string map {-stroke -outline} [join $args]]
                        # puts "  -> \$args: $args"
                        $TabsFrame_Canvas itemconfigure  $item   {*}$args
                    }
                    line {
                        set args [string map {-stroke -fill} [join $args]]
                        # puts "  -> \$args: $args"
                        $TabsFrame_Canvas itemconfigure  $item   {*}$args
                    }
                    default {
                        $TabsFrame_Canvas itemconfigure  $item   {*}$args
                    }
                }
            }
            default {}
        }        
    
    }
        #
    method UpdateTabButton {} {
            #
            # puts "UpdateTabButton:"
            # puts "    -> [winfo children $TabsFrame_Button]"
        foreach child [winfo children $TabsFrame_Button] {
            destroy $child
        }
            #
        set tabDef  [$ObjState getMap_VisibleTabIDs]
            # puts "        .. \$tabDef $tabDef"
        foreach {window name} $tabDef {
            my CreateButton $window $name
        }
    }
    method CreateButton {w text} {
            #
            # puts "\n -----------------------\n"
            #
            # puts [winfo children $TabsFrame_Button]
        set id     $idNext
        set idNext [expr {$idNext + 1}]
        set button  [ttk::button $TabsFrame_Button.__btn__$id -text $text]
          # button  [button $TabsFrame_Button.__btn__$id -text $text]
            # puts "    ... add a button: $button  <- $text at position $id"
        $button configure -command [list [self] select $w]
            #
        grid $button
        grid configure $button  -row 0 -column $id
            #
            #
        dict set DictRegistryButton $w [list name $text button $w]
            #
    }
        #
}
    #
    # http://chiselapp.com/user/rene/repository/tkpath/doc/trunk/doc/tkpath.n.md
    # https://wiki.tcl-lang.org/page/tkpath
    # https://wiki.tcl-lang.org/page/Examples+of+drawing+with+Tkpath
    #